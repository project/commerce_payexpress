(function ($, Drupal, drupalSettings) {
  'use strict';

  function cargarFrame(params) {
    GSCheckOut.load(
      "payExpressIframePago", params.base_url + "?apikey=" + params.apikey + "&amount=" +
      params.amount + "&cuotas=" + params.cuotas + "&currency=" + params.currency + "&sello=" + params.sello + "&name=" + params.name + "&email=" + params.email + "&doc=" + params.documento
    );
  }

  GSCheckOut.Bind("tokenCreated", function (token) {
    var params = JSON.parse(drupalSettings.commerce_payexpress);
    $.ajax({
      url: "/payexpress/post",
      type: 'POST',
      data: {
        token: token,
        transactionId: params.transactionId,
        amount: params.amount
      },
      success: function (data) {
        // @todo use commerce_payexpress.payment_complete routing.
        window.location.href = '/pago-completado?transactionId=' + params.transactionId;
      },
      error: function (data) {
        var message = '';
        if (data.responseJSON.message) {
          message = data.responseJSON.message;
        }
        // @todo use commerce_payexpress.payment_error routing.
        window.location.href = '/error-pago/?transactionId=' + params.transactionId + '&message=' + message;
      }
    });
  });

  Drupal.behaviors.offsiteForm = {
    attach: function (context) {
      cargarFrame(JSON.parse(drupalSettings.commerce_payexpress));
    }

  };

}(jQuery, Drupal, drupalSettings));
