(function (Drupal, once) {
  Drupal.behaviors.validate_documento = {
    attach(context) {
      const elements = once('validate-ci', '#edit-commerce-payexpress-pane-custom-message-documento', context);
      elements.forEach(el => {
        el.addEventListener('input', () => validate_ci(el));
      });
    },
  };

  function validate_ci(el) {
    var a = el.value.replace(/\D/g, '');
    a[a.length - 1] == function (a) {
      var e = 0, i = 0;
      if (a.length <= 6) {
        for (i = a.length; i < 7; i++) {
          a = '0' + a;
        }
      }
      for (i = 0; i < 7; i++) {
        e += parseInt('2987634'[i]) * parseInt(a[i]) % 10;
      }
      return e % 10 == 0 ? 0 : 10 - e % 10;
    }(a = a.replace(/[0-9]$/, '')) ? (el.classList.remove('is-invalid'), el.classList.add('is-valid')) : (el.classList.remove('is-valid'), el.classList.add('is-invalid'));
  }
}(Drupal, once));
