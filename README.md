# Commerce PayExpress

This module integrates Plexo with Drupal Commerce.

## Requirements

This module requires the following modules:

- [Commerce Payment](https://www.drupal.org/project/commerce)
- [Mask Field](https://www.drupal.org/project/mask)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Go to the payment methods section: Administration » Commerce » Configuration » 
Payment gateways and add payment gateway with PayExpress.
