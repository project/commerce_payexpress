<?php

namespace Drupal\commerce_payexpress;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;

/**
 * Class PayExpress Service.
 */
class PayExpressService {

  use StringTranslationTrait;

  /**
   * The application settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a SidekickService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $httpClient) {
    $this->settings = $config_factory->get('commerce_payexpress.settings');
    $this->httpClient = $httpClient;
  }

  /**
   * Invocación del método REST para autorizar la compra.
   */
  public function purchase(string $base_url, string $token, string $amount, string $transactionId, object $facturaDTO, string $currency): array {
    $url = $base_url . 'sale/save';

    try {
      $response = $this->httpClient->request('POST', $url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'user' => $this->settings->get('user'),
          'password' => $this->settings->get('password'),
        ],
        'body' => json_encode([
          'origenTransaccion' => '2',
          'amount' => $amount,
          'currencyCode' => $currency,
          'token' => $token,
          'transactionId' => $transactionId,
          'apiKey' => $this->settings->get('api_key'),
          'factura' => $facturaDTO->toArray(),
        ]),
      ]);

      $statusCode = $response->getStatusCode();
      $body = json_decode($response->getBody(), TRUE);

      // 1) Controlo Error Grave en PayExpress.
      if ($statusCode !== 200) {
        return [
          'message' => $this->t('Error al procesar el pago. Intente luego'),
          'statusCode' => 400,
          'error' => 1,
        ];
      }

      // 2) Controlo Error en la respuesta de PayExpress.
      if ($body['errorCode'] !== '00') {
        return [
          'message' => $body['errorMessage'],
          'statusCode' => 400,
          'error' => 2,
        ];
      }

      // 3) Controlo Error en la respuesta del emisor de la tarjeta.
      if ($body['issuerResponse']['issuerResponseCode'] !== '00') {
        return [
          'message' => $body['issuerResponse']['issuerResponseMessage'],
          'statusCode' => 400,
          'error' => 3,
        ];
      }

      return [
        'message' => $this->t('Pago realizado con éxito'),
        'statusCode' => 200,
        'error' => 0,
      ];
    }
    catch (\Exception $e) {
      return [
        'message' => $e->getMessage(),
        'statusCode' => $e->getResponse()->getStatusCode(),
      ];
    }
  }

}
