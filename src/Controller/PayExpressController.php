<?php

namespace Drupal\commerce_payexpress\Controller;

use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * PayExpress Controller.
 */
class PayExpressController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new DummyRedirectController object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Callback method which accepts POST.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function post() {
    $token = $this->currentRequest->request->get('token');
    $amount = $this->currentRequest->request->get('amount');
    $transactionId = $this->currentRequest->request->get('transactionId');
    $logger = \Drupal::logger('commerce_payexpress');
    $logger->info('Token: ' . $token);
    $order = \Drupal::entityTypeManager()
      ->getStorage('commerce_order')
      ->load($transactionId);

    if (!$order) {
      return new JsonResponse(['error' => 'Pedido no encontrado'], Response::HTTP_NOT_FOUND);
    }
    $payment_gateway = $order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $baseUrl = $payment_gateway_plugin->getBaseUrl();
    $currencyCode = $order->getTotalPrice()->getCurrencyCode();
    $facturaDto = new FacturaDTO();
    $facturaDto->numero = $order->id();
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $billing_address = $order->getBillingProfile()->get('address')->first();
    $facturaDto->nombre = $order->getData('order_nombre');
    $facturaDto->documento = $order->getData('order_documento');
    $facturaDto->domicilio = $billing_address->getAddressLine1();
    $facturaDto->ciudad = $billing_address->getLocality();
    $facturaDto->departamento = $billing_address->getAdministrativeArea();
    $facturaDto->pais = $billing_address->getCountryCode();
    $facturaDto->montoGravado = round(($amount / 1.22), 2) * 100;
    $facturaDto->montoTotal = $amount;
    $facturaDto->montoPagar = $amount;
    $facturaDto->tipoMoneda = $currencyCode;

    $payExpressService = \Drupal::service('commerce_payexpress.service');
    $response = $payExpressService->purchase($baseUrl, $token, $amount, $transactionId, $facturaDto, $currencyCode);

    switch ($response['statusCode']) {
      case 200:
        $logger->info('Pago exitoso: ' . $transactionId);
        $order->set('state', 'completed');
        $order->save();
        $current_user = $order->getCustomerId();
        $payment = Payment::create([
          'type' => 'payment_default',
          'payment_gateway' => 'payexpress',
          'order_id' => $order->id(),
          'amount' => [
            'number' => $amount,
            'currency_code' => $currencyCode,
          ],
          'state' => 'completed',
          'payment_gateway_mode' => $payment_gateway_plugin->getMode(),
          'uid' => $current_user,
        ]);
        $payment->save();
        break;

      case 400:
        $logger->error('Error al procesar el pago: ' . $transactionId . ' - ' . $response['message']);
        $order->set('state', 'pending');
        $order->save();
        break;
    }

    return new JsonResponse($response, $response['statusCode']);
  }

  /**
   * HTML Page with message complete payment.
   */
  public function completePayment() {
    $transactionId = $this->currentRequest->query->get('transactionId');
    $logger = \Drupal::logger('commerce_payexpress');
    $logger->info('Complete payment: ' . $transactionId);
    return [
      '#markup' => $this->t('Complete payment'),
    ];
  }

  /**
   * HTML Page with message error payment.
   */
  public function errorPayment() {
    $transactionId = $this->currentRequest->query->get('transactionId');
    $message = $this->currentRequest->query->get('message');
    $logger = \Drupal::logger('commerce_payexpress');
    $logger->error('Error payment: ' . $transactionId . ' - ' . $message);
    return [
      '#markup' => $this->t('Error payment') . ' - ' . $message,
    ];
  }

}

/**
 * Class Factura DTO.
 */
class FacturaDTO {

  /**
   * Tipo de operación.
   *
   * @var string
   *   $tipoOperacion
   */
  public string $tipoOperacion = 'C';

  /**
   * Tipo de comprobante.
   *
   * @var string
   *    $tipoComprobante
   */
  public string $tipoComprobante = '111';

  /**
   * Serie de la factura.
   *
   * @var string
   *    $serie
   */
  public string $serie = 'AA';

  /**
   * Número de factura.
   *
   * @var string
   *    $numero
   */
  public string $numero;

  /**
   * Tipo de documento.
   *
   * @var string
   *    $tipoDocumento
   */
  public string $tipoDocumento = '3';

  /**
   * Número de documento.
   *
   * @var string
   *    $documento
   */
  public string $documento;

  /**
   * Nombre.
   *
   * @var string
   *    $nombre
   */
  public string $nombre;

  /**
   * Domicilio.
   *
   * @var string
   *    $domicilio
   */
  public string $domicilio;

  /**
   * Ciudad.
   *
   * @var string
   *    $ciudad
   */
  public string $ciudad;

  /**
   * Departaento.
   *
   * @var string
   *    $departamento
   */
  public string $departamento;

  /**
   * Código de país.
   *
   * @var string
   *    $pais
   */
  public string $pais;

  /**
   * Tipo de moneda.
   *
   * @var string
   *    $tipoMoneda
   */
  public string $tipoMoneda;

  /**
   * Monto gravado.
   *
   * @var string
   *    $montoGravado
   */
  public string $montoGravado;

  /**
   * Monto total.
   *
   * @var string
   *    $montoTotal
   */
  public string $montoTotal;

  /**
   * Monto a pagar.
   *
   * @var string
   *    $montoPagar
   */
  public string $montoPagar;

  /**
   * FacturaDTO constructor.
   */
  public function __construct() {}

  /**
   * Convert FacturaDTO to array.
   *
   * @return array
   *   Array with FacturaDTO values.
   */
  public function toArray(): array {
    return [
      'tipoOperacion' => $this->tipoOperacion,
      'tipoComprobante' => $this->tipoComprobante,
      'serie' => $this->serie,
      'numero' => $this->numero,
      'tipoDocumento' => $this->tipoDocumento,
      'documento' => $this->documento,
      'nombre' => $this->nombre,
      'domicilio' => $this->domicilio,
      'ciudad' => $this->ciudad,
      'departamento' => $this->departamento,
      'pais' => $this->pais,
      'tipoMoneda' => $this->tipoMoneda,
      'montoGravado' => $this->montoGravado,
      'montoTotal' => $this->montoTotal,
      'montoPagar' => $this->montoPagar,
    ];
  }

}
