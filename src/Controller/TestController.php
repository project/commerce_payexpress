<?php

namespace Drupal\commerce_payexpress\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides test for PayExpress.
 *
 * @package Drupal\commerce_payexpress\Controller
 */
class TestController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Return HtmlResponse.
   */
  public function index(): array {
    $gatewayStorage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
    $gateways = $gatewayStorage->loadByProperties([
      'plugin' => 'payexpress',
    ]);
    $plugin = reset($gateways);
    $plugin = $plugin->getPlugin();
    $result = $plugin->getApiKey();

    return [
      '#type' => 'markup',
      '#markup' => '<pre>' . $result . '</pre>',
      '#attached' => [
        'library' => [
          'commerce_payexpress/testing',
        ],
      ],
    ];
  }

}
