<?php

namespace Drupal\commerce_payexpress\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PayExpress Payment Form.
 *
 * @package Drupal\commerce_payexpress\PluginForm
 */
class PayExpressPaymentForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $config = \Drupal::config('commerce_payexpress.settings');

    $data['apikey'] = $config->get('api_key');
    $data['base_url'] = $payment->getPaymentGateway()->getPlugin()->getBaseUrl();

    // Payment data.
    $data['currency'] = $payment->getAmount()->getCurrencyCode();
    $data['amount'] = $this->getTotalFormatted($payment->getAmount()->getNumber());
    $data['variables[payment_gateway]'] = $payment->getPaymentGatewayId();
    $data['transactionId'] = $payment->getOrderId();

    // Order and billing address.
    $order = $payment->getOrder();
    $data['documento'] = $order->getData('order_documento');
    $data['cuotas'] = $order->getData('order_cuotas');
    $data['sello'] = $order->getData('order_card');
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $billing_address = $order->getBillingProfile()->get('address')->first();
    $data['name'] = $order->getData('order_nombre');
    $data['email'] = $order->getEmail();
    $data['city'] = $billing_address->getLocality();
    $data['state'] = $billing_address->getAdministrativeArea();

    // Form url values.
    $data['continueurl'] = $form['#return_url'];
    $data['cancelurl'] = $form['#cancel_url'];

    $data = json_encode($data);

    if ($payment->getPaymentGateway()->getPlugin()->getMode() === 'live') {
      $form['#attached']['library'][] = 'commerce_payexpress/checkout';
    }
    else {
      $form['#attached']['library'][] = 'commerce_payexpress/checkout-testing';
    }
    $form['#attached']['drupalSettings']['commerce_payexpress'] = $data;

    $form['payExpressIframePago'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => "Cargando...",
      '#attributes' => [
        'id' => 'payExpressIframePago',
        'style' => 'width: 915px; height: 780px; overflow: hidden;',
        'scrolling' => 'no',
      ],
    ];
    return $form;
  }

  /**
   * Get total formatted.
   */
  public function getTotalFormatted($amount): string {
    return str_replace('.', '', number_format($amount, 2, '.', ''));
  }

}
