<?php

namespace Drupal\commerce_payexpress\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a custom message pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_payexpress_pane_custom_message",
 *   label = @Translation("PayExpress Info"),
 * )
 */
class CustomMessagePane extends CheckoutPaneBase {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['#attached']['library'][] = 'commerce_payexpress/validate-documento';
    $nombre_completo = $this->order->getData('order_nombre');
    if (!$nombre_completo) {
      $nombre_completo = $this->cleanName($this->order->getBillingProfile()->get('address')->first()->getGivenName() . ' ' . $this->order->getBillingProfile()->get('address')->first()->getFamilyName());
    }
    $pane_form['nombre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre como figura en la tarjeta de crédito'),
      '#default_value' => $nombre_completo ?: '',
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'name',
      ],
    ];
    $documento = $this->order->getData('order_documento');
    $pane_form['documento'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cédula de identidad'),
      '#description' => $this->t('Ejemplo: 5.123.456-7.'),
      '#default_value' => $documento ?: '',
      '#required' => TRUE,
      '#attributes' => [
        // pattern allow numbers and dot and dash, ex: 3.583.291-1
        'pattern' => '^[0-9]{1,3}(\.[0-9]{3}){2}-[0-9]$',
        'title' => $this->t('Ingrese una cédula de identidad uruguaya válida'),
        'autocomplete' => 'ci',
        'inputmode' => 'numeric',
      ],
      '#mask' => [
        'value' => '0.000.000-0',
        'reverse' => FALSE,
        'selectonfocus' => FALSE,
        'clearifnotmatch' => TRUE,
      ],
      '#maxlength' => 11,
      '#size' => 11,
    ];
    $payment_method = $this->order->get('payment_gateway')->entity->getPluginId();
    if ($payment_method === 'payexpress') {
      $cards = $this->order->get('payment_gateway')->entity->getPlugin()
        ->getActiveCards();
      if (count($cards)) {
        $pane_form['card'] = [
          '#type' => 'select',
          '#title' => $this->t('Card'),
          '#options' => $cards,
          '#default_value' => $this->order->getData('order_card') ?: array_key_first($cards),
          '#required' => TRUE,
          '#attributes' => [
            'autocomplete' => 'cc-type',
          ],
        ];
        foreach ($cards as $card => $card_name) {
          $cuotas = $this->order->get('payment_gateway')->entity->getPlugin()
            ->getCuotasCard($card);
          if (count($cuotas)) {
            $pane_form[$card]['cuotas'] = [
              '#type' => 'select',
              '#title' => $this->t('Cuotas'),
              '#options' => $cuotas,
              '#default_value' => $this->order->getData('order_cuotas') ?: array_key_first($cuotas),
              '#required' => TRUE,
              '#attributes' => [
                'autocomplete' => 'cc-cuotas',
              ],
              '#states' => [
                'visible' => [
                  ':input[name="commerce_payexpress_pane_custom_message[card]"]' => ['value' => $card],
                ],
              ],
            ];
          }
        }
      }
    }
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneSummary() {
    $order_nombre = $this->order->getData('order_nombre');
    $order_documento = $this->order->getData('order_documento');
    if ($order_nombre || $order_documento) {
      return [
        '#plain_text' => $order_nombre . ' ' . $order_documento,
      ];
    }
    return [];
  }

  /**
   * Validate the pane form.
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    if (!$this->validateName($values['nombre'])) {
      $form_state->setError($pane_form['nombre'], $this->t('El nombre solo puede contener letras del abecedario en mayúsculas y minúsculas, espacios y tildes.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    if (isset($values['documento'])) {
      // Remove dots and dashes from the document.
      $values['documento'] = preg_replace('/[.-]/', '', $values['documento']);
      $this->order->setData('order_documento', $values['documento']);
    }
    if (isset($values['nombre'])) {
      $this->order->setData('order_nombre', $values['nombre']);
    }
    if (isset($values['card'])) {
      $this->order->setData('order_card', $values['card']);
      if (isset($values[$values['card']]['cuotas'])) {
        $this->order->setData('order_cuotas', $values[$values['card']]['cuotas']);
      }
    }
  }

  /**
   * Validar el nombre.
   *
   * Si solo contiene letras del abecedario en mayúsculas y minúsculas,
   * espacios y tildes.
   */
  public function validateName($value) {
    return preg_match('/^[A-Za-záéíóúüÁÉÍÓÚÜ\s]+$/u', $value);
  }

  /**
   * Limpiar el nombre.
   *
   * Para que solo contenga letras del abecedario en mayúsculas y minúsculas,
   * espacios y tildes.
   */
  public function cleanName($value) {
    return preg_replace('/[^A-Za-záéíóúüÁÉÍÓÚÜ\s]+/u', '', $value);
  }

}
