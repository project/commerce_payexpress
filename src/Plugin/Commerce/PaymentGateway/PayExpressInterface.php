<?php

namespace Drupal\commerce_payexpress\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;

/**
 * Provides the interface for the PayExpress payment gateway.
 */
interface PayExpressInterface extends OffsitePaymentGatewayInterface {

  /**
   * Get Active Cards.
   */
  public function getActiveCards();

  /**
   * Get Cuotas Card.
   */
  public function getCuotasCard($card);

  /**
   * Get Base Url.
   */
  public function getBaseUrl();

}
