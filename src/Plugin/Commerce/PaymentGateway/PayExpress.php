<?php

namespace Drupal\commerce_payexpress\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "payexpress",
 *   label = @Translation("PayExpress (Off-site redirect)"),
 *   display_label = @Translation("PayExpress"),
 *   modes = {
 *     "test" = @Translation("Sandbox"),
 *     "live" = @Translation("Production"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_payexpress\PluginForm\PayExpressPaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "anda",
 *     "creditel",
 *     "creditos_directos",
 *     "oca",
 *     "cabal",
 *     "club_del_este",
 *     "mastercard",
 *     "visa",
 *     "passcard",
 *    },
 *    requires_billing_information = FALSE,
 * )
 */
class PayExpress extends OffsitePaymentGatewayBase implements PayExpressInterface {

  const PAYEXPRESS_PROD_URL = 'https://sistemas.payexpress.com.uy/';

  const PAYEXPRESS_TEST_URL = 'https://testing.payexpress.com.uy/';

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    if ($this->getMode() === 'live') {
      return self::PAYEXPRESS_PROD_URL;
    }
    else {
      return self::PAYEXPRESS_TEST_URL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveCards() {
    $cards = [];
    foreach ($this->configuration['cards'] as $card => $card_values) {
      if ($card_values['active']) {
        $cards[$card] = $this->getCardName($card);
      }
    }
    return $cards;
  }

  /**
   * {@inheritdoc}
   */
  private function isActiveCard($card) {
    if (!isset($this->configuration['cards'][$card]['active'])) {
      return FALSE;
    }
    return $this->configuration['cards'][$card]['active'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCuotasCard($card) {
    if (!isset($this->configuration['cards'][$card]['cuotas'])) {
      return [];
    }
    foreach ($this->configuration['cards'][$card]['cuotas'] as $cuota => $cuota_value) {
      if (!$cuota_value) {
        unset($this->configuration['cards'][$card]['cuotas'][$cuota]);
      }
    }
    return $this->configuration['cards'][$card]['cuotas'];
  }

  /**
   * {@inheritdoc}
   */
  private function getCuotasOptions(): array {
    $cuotas = [];
    for ($i = 1; $i <= 12; $i++) {
      $cuotas[$i] = $i;
    }
    return $cuotas;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['cards'] = [
      '#type' => 'details',
      '#title' => $this->t('Cards'),
      '#open' => TRUE,
    ];

    $form['cards']['anda'] = [
      '#type' => 'details',
      '#title' => $this->t('Anda'),
      '#open' => TRUE,
    ];

    $form['cards']['anda']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->isActiveCard('anda'),
      '#disabled' => TRUE,
    ];

    $form['cards']['anda']['cuotas'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cuotas'),
      '#options' => $this->getCuotasOptions(),
      '#default_value' => $this->getCuotasCard('anda'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[payexpress][cards][anda][active]"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
      '#disabled' => TRUE,
    ];

    $form['cards']['creditel'] = [
      '#type' => 'details',
      '#title' => $this->t('Creditel'),
      '#open' => TRUE,
    ];

    $form['cards']['creditel']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->isActiveCard('creditel'),
    ];

    $form['cards']['creditel']['cuotas'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cuotas'),
      '#options' => $this->getCuotasOptions(),
      '#default_value' => $this->getCuotasCard('creditel'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[payexpress][cards][creditel][active]"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    ];

    $form['cards']['creditos_directos'] = [
      '#type' => 'details',
      '#title' => $this->t('Créditos directos'),
      '#open' => TRUE,
    ];

    $form['cards']['creditos_directos']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->isActiveCard('creditos_directos'),
      '#disabled' => TRUE,
    ];

    $form['cards']['creditos_directos']['cuotas'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cuotas'),
      '#options' => $this->getCuotasOptions(),
      '#default_value' => $this->getCuotasCard('creditos_directos'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[payexpress][cards][creditos_directos][active]"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
      '#disabled' => TRUE,
    ];

    $form['cards']['oca'] = [
      '#type' => 'details',
      '#title' => $this->t('Oca'),
      '#open' => TRUE,
    ];

    $form['cards']['oca']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->isActiveCard('oca'),
    ];

    $form['cards']['oca']['cuotas'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cuotas'),
      '#options' => $this->getCuotasOptions(),
      '#default_value' => $this->getCuotasCard('oca'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[payexpress][cards][oca][active]"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    ];

    $form['cards']['cabal'] = [
      '#type' => 'details',
      '#title' => $this->t('Cabal'),
      '#open' => TRUE,
    ];

    $form['cards']['cabal']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->isActiveCard('cabal'),
      '#disabled' => TRUE,
    ];

    $form['cards']['cabal']['cuotas'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cuotas'),
      '#options' => $this->getCuotasOptions(),
      '#default_value' => $this->getCuotasCard('cabal'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[payexpress][cards][cabal][active]"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
      '#disabled' => TRUE,
    ];

    $form['cards']['club_del_este'] = [
      '#type' => 'details',
      '#title' => $this->t('Club del este'),
      '#open' => TRUE,
    ];

    $form['cards']['club_del_este']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->isActiveCard('club_del_este'),
    ];

    $form['cards']['club_del_este']['cuotas'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cuotas'),
      '#options' => $this->getCuotasOptions(),
      '#default_value' => $this->getCuotasCard('club_del_este'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[payexpress][cards][club_del_este][active]"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    ];

    $form['cards']['visa'] = [
      '#type' => 'details',
      '#title' => $this->t('Visa'),
      '#open' => TRUE,
    ];

    $form['cards']['visa']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->isActiveCard('visa'),
    ];

    $form['cards']['visa']['cuotas'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cuotas'),
      '#options' => $this->getCuotasOptions(),
      '#default_value' => $this->getCuotasCard('visa'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[payexpress][cards][visa][active]"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    ];

    $form['cards']['master'] = [
      '#type' => 'details',
      '#title' => $this->t('Master'),
      '#open' => TRUE,
    ];

    $form['cards']['master']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->isActiveCard('master'),
    ];

    $form['cards']['master']['cuotas'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cuotas'),
      '#options' => $this->getCuotasOptions(),
      '#default_value' => $this->getCuotasCard('master'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[payexpress][cards][master][active]"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    ];

    $form['cards']['passcard'] = [
      '#type' => 'details',
      '#title' => $this->t('PassCard'),
      '#open' => TRUE,
    ];

    $form['cards']['passcard']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->isActiveCard('passcard'),
      '#disabled' => TRUE,
    ];

    $form['cards']['passcard']['cuotas'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cuotas'),
      '#options' => $this->getCuotasOptions(),
      '#default_value' => $this->getCuotasCard('passcard'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[payexpress][cards][passcard][active]"]' => ['checked' => TRUE],
        ],
      ],
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
      '#disabled' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      foreach ($values['cards'] as $card => $card_values) {
        $this->configuration['cards'][$card] = $card_values;
      }
    }
  }

  /**
   * Mapping cards names.
   */
  public function getCardName($card) {
    $cards = [
      'anda' => 'Anda',
      'creditel' => 'Creditel',
      'creditos_directos' => 'Créditos Directos',
      'oca' => 'Oca',
      'cabal' => 'Cabal',
      'club_del_este' => 'Club del Este',
      'visa' => 'Visa',
      'master' => 'Master',
      'passcard' => 'PassCard',
    ];
    return $cards[$card];
  }

}
